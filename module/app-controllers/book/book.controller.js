(function () {
    'use strict';

    angular
        .module('app')
        .controller('BookController', BookController);

    BookController.$inject = ['UserService', 'BookService', '$routeParams', '$rootScope','$scope'];
    function BookController(UserService, BookService, $routeParams, $rootScope, $scope) {
        var vm = this;

        initController();

        function initController() { 
            LoadSinglePage();
            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
        }

        function LoadSinglePage(){
            var slug = $routeParams.slug;
            BookService.Book(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                console.log(res);    
            });
        }
        
    }

})();