(function () {
    'use strict';

    angular
        .module('app')
        .controller('NotificationController', NotificationController);

    NotificationController.$inject = ['UserService', 'NotificationService', '$rootScope', '$routeParams', '$scope','ngToast','$location'];
    function NotificationController(UserService, NotificationService, $rootScope, $routeParams, $scope,ngToast,$location) {
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.isProccessing=false;
        $scope.isDataFound=false;
        $scope.user = {};
        var user='';
        initController();

        function initController() {
            $scope.loadingImage="././images/loader.gif";
            var slug = $routeParams.slug;
            user = JSON.parse(window.localStorage.getItem('user'));
            $scope.user.first_name = user.first_name;
            $scope.user.last_name = user.last_name;
            if(slug == undefined){
                loadNotification();
            }else{
                //LoadSinglePage(slug);
            }
        }

        function loadNotification()
        {
            $scope.isProccessing = true;
            NotificationService.Notifications().then(function (response){
                if(response.status==200){
                    $scope.isProccessing = false;
                    if(response.data.Data.length > 0){
                        $scope.isDataFound = true;
                        $scope.list = response.data.Data;
                        $rootScope.notificationCount=$scope.list.length;
                    }
                    else{
                        $scope.isDataFound = false;
                    }
                }
            });

            // NotificationService.Notifications($scope.lastpage).then(function (response){
            //     var res = response;

            //     if(res.data.status == 1){
            //         if(res.data.result.current_page == res.data.result.last_page)
            //         {
            //             $scope.is_disable = true;
            //         }
            //         $scope.items = res.data.result.data;
            //         console.log($scope.items);
            //     }
                
            //     if(res.status == 0){
            //         alert("Something went wrong");
            //     }
            // });
        }

        function LoadSinglePage(slug){
            
            ScholarshipService.Scholarship(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                console.log(res);    
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $scope.isProccessing=true;
                NotificationService.Notifications($scope.lastpage).then(function (response){
                    var res = response;
                    $scope.isProccessing=false;
                    if(res.data.status == 1){
                        if(res.data.current_page == res.data.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        $scope.totalCollege=res.data.to;
                        $scope.newItems = $scope.newItems.concat(res.data.result);
                    }
                    if(res.status == 0){
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
        }
    }

})();