﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService', '$sce', '$location','AuthenticationService','$rootScope', 'FlashService','$scope', '$timeout', '$q'];
    function RegisterController(UserService, $sce, $location,AuthenticationService, $rootScope, FlashService,$scope, $timeout, $q) {
        
        $scope.selected = undefined;
        
        $scope.primarySchool = [];
        $scope.stateList     = [];
        $scope.colleges      = [];

        var vm = this;
        $scope.state = "";
        $scope.primary_school = "";
        $scope.step1 = true;
        $scope.step2 = false;
        vm.register = register;
        vm.ForgetPasswordRequest = ForgetPasswordRequest;
        vm.ResetPasswordRequest = ResetPasswordRequest;

        initController();
        
        function initController() {

            AuthenticationService.ClearCredentials();
            $rootScope.isUserLoggedIn = false;

            UserService.GetStateEducationlevelCollege()
                .then(function (response) {
                    
                    if (response.status == 1 ) {

                        angular.forEach(response.education_levels, function(value, key) {
                          this.push(value.education_level);
                        }, $scope.primarySchool);

                        angular.forEach(response.states, function(value, key) {
                          this.push(value.name);
                        }, $scope.stateList);

                        angular.forEach(response.colleges, function(value, key) {
                            this.push(value);
                        }, $scope.colleges);

                        console.log($scope.colleges);
                        
                    } else {
                        $scope.primarySchool = ['High School', 'Intermediate', 'Bachelor','Master Degree'];
                        $scope.stateList = ['California', 'Florida', 'texas','Hawaii'];
                        $scope.colleges = ['you can add new college'];
                    }
                });

            primarySchoolLoad();
            stateLoad();
            
        }

        function primarySchoolLoad() {

            $scope.primary_school = $scope.primarySchool[0];
            $scope.primarySchoolOptions = {
                onSelect: function (item) {
                    $scope.primary_school = item;
                }
            };
            $scope.nestedItemsLevel2 = [];
            $scope.primarySchoolOptions.onSelect($scope.primarySchool[0]);
        }

        function stateLoad() {

            $scope.state = $scope.stateList[0];
            $scope.stateListOptions = {
                onSelect: function (item) {
                    $scope.state = item;
                }
            };
            $scope.nestedItemsLevel2 = [];
            $scope.stateListOptions.onSelect($scope.stateList[0]);
        }

        $scope.goToStep2 = function() {
            $scope.step1 = false;
            $scope.step2 = true;
        }

        $scope.goToStep1 = function() {
            $scope.step1 = true;
            $scope.step2 = false;
        }

        function register() {
            
            vm.user.state = $scope.state;
            vm.user.education_level = $scope.primary_school;
            $scope.validation = {};
            
            if(vm.user.state == '' || vm.user.state == undefined)
            {
                $scope.validation['state'] = "State is required*";
            }

            if(vm.user.education_level == '' || vm.user.education_level == undefined)
            {
                $scope.validation['education_level'] = "Education Level is required*";
            }

            if(vm.user.college == '' || vm.user.college == undefined)
            {
                $scope.validation['college'] = "College is required*";
            }

            if(vm.user.gender == '' || vm.user.gender == undefined)
            {
                $scope.validation['gender'] = "Gender is required*";
            }

            if(!$.isEmptyObject($scope.validation))
            {
                var errorsHtml = '<ul>';
                angular.forEach($scope.validation, function(key,value){
                    errorsHtml += "<li>"+ key + "</li>";
                });
                errorsHtml += "</ul>";
                console.log(errorsHtml);
                FlashService.Error(errorsHtml);
                return false;
            }


            vm.dataLoading = true;
            UserService.Create(vm.user)
                .then(function (response) {
                    console.log(response.message);
                    if (response.status == 1) {
                        FlashService.Success('Registration successful', true);
                        $location.path('/login');
                    } else if(response.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.message,function(key,value){
                            errorsHtml += "<li>"+ response[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in registration process please contact to admin");
                        vm.dataLoading = false;
                    }
                });
        }

        function ForgetPasswordRequest () {
            
            UserService.ForgetPasswordRequest(vm.user)
                .then(function (response){
                    console.log(response);
                })
        }

        function ResetPasswordRequest () {
            
            UserService.ResetPasswordRequest(vm.user)
                .then(function (response){
                    console.log(response);
                })
        }

        $scope.withoutSanitize = function (message) {
                return $sce.trustAsHtml(message);
            };
    }

})();
