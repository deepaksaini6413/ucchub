var message = angular.module('MsgCtrl', []);

message.controller('MessageController',function($scope, $routeParams, $window, Login, MAIN, $auth, BUYER, Login, $sessionStorage, $location, $cookieStore, FileUploader, $timeout, $interval){
	$scope.loading = true;
  	var profile = Login.getProfile();
  	profile.error(function(response) {
	    swal('Login Expire', 'Your login session is expire', 'error');
	    //make user logout
	    var logout = Login.logout();
	    logout.success(function(){
	        $cookieStore.remove('auth');
	        $rootScope = null;
	        $location.path('/');
	    });
	  });
  	profile.success(function(response){
	    if (response.status == 0) {
	      $cookieStore.remove('auth');
	      $location.path('/');
	    }

	   

	    // console.log(response.profile[0].user_type);
	    if (Login.checkAuth()) {
	        var auth              = $cookieStore.get('auth');
	        $scope.first_name     = auth.first_name;
	        $scope.last_name      = auth.last_name;
	        $scope.email          = auth.email;
	        $scope.profile_picture= auth.profile_picture;
	        $scope.user_type      = auth.user_type;
	        $scope.profile_picture= auth.profile_picture;
	        $scope.letter         = $scope.first_name.charAt(0);
	    }
	});
  	$scope.emojiMessage={};
	if ($location.path().split('/')[1] == 'inbox') {
		var group_id = $location.path().split('/')[2];
		var getChat = MAIN.getChat({group_id: group_id});
		getChat.success(function(response) {
			$scope.loading = false;
			$scope.receiver = response.message.receiver;
			$scope.sender = response.message.sender;
			if (response.message.receiver.email == $scope.email) {
				$scope.sender = response.message.receiver;
				$scope.receiver = response.message.sender;
			}else if(response.message.sender.email == $scope.email){
				$scope.sender = response.message.sender;
				$scope.receiver = response.message.receiver;
			}
			$scope.chatStream = response.message.message;
			$scope.group_id = $routeParams.group_id;
			$sessionStorage.receiver_key = $scope.receiver.user_type;

			//autorun listner for the receiving messages
			$interval(function (argument) {
				var receive = MAIN.receiveMsg({group_id: $routeParams.group_id});
				receive.success(function(response) {
					if (response.status == 1) {
						angular.forEach(response.receive, function(value, key) {
						  	$scope.chatStream.push(value);
						  	var list = $('#message');
			        		var scrollHeight = list.prop('scrollHeight')
					        list.animate({scrollTop: scrollHeight}, 500);
						});
					}
				});
			}, 3000);
		});


		
	    $scope.sendMsg = function() {
	      	var data = {group_id: $routeParams.group_id, message: $scope.emojiMessage.rawhtml};
	        var send = MAIN.sendMessage(data);
	        send.success(function(response) {
	        	console.log(response);
	        	if (response.status == 1) {
	        		$scope.emojiMessage.rawhtml = null;
	        		if (response.fileMsg.length != 0) {
	        			$scope.chatStream.push(response.fileMsg);
	        		}
	        		if (response.message.length != 0) {
	        			console.log(response.message.length);
	        			$scope.chatStream.push(response.message);
	        		}
	        		$scope.fileThumbnail = false;
	        		var list = $('#message');
	        		var scrollHeight = list.prop('scrollHeight')
			        list.animate({scrollTop: scrollHeight}, 500);
	        	}
	        });
	    }

	    $scope.moveArchive = function(stream) {
	      var makeArchive = MAIN.makeArchive({group_id: stream});
	      makeArchive.success(function(response) {
	        if (response.status == 1) {
	        	if ($scope.user_type == 1) {
	            	$location.path('/buyer/inbox');
	        	}else{
	            	$location.path('/printer/inbox');

	        	}
	        }
	      });
	     }

	    $scope.scrollDown = function() {
	    	console.log('make scroll');
	    	//sroll message to the bottom
	    	 $timeout(function() {
	    	 	
				var list = $('#message');
				console.log('list', list);
				var scrollHeight = list.prop('scrollHeight');
				console.log('scrollHeight', message);
		        console.log('action',list.animate({scrollTop: scrollHeight}, 100));
	    	 }, 1000);
	    }
	    /**
	     * send file
	     */
	    var uploader = $scope.uploader = new FileUploader({
	      url: 'api/send-msg-file?_token='+ $('meta[name="token"]').attr('content')+'&g='+$routeParams.group_id,
	      autoUpload : true
	  	});
	
		// FILTERS

		uploader.filters.push({
		  name: 'imageFilter',
		  fn: function(item /*{File|FileLikeObject}*/, options) {
		      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
		      return '|jpg|png|jpeg|bmp|gif|pdf|doc|docx|xls|vnd.openxmlformats-officedocument.wordprocessingml.document'.indexOf(type) !== -1;
		  }
		});
		var manageImages = [];
		// CALLBACKS

		uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
			console.log('filter', filter);
			swal('Invalid file formate', "Please add files having .jpg, .jpeg, .png, .bmp extension.", 'error');
		  // console.info('onWhenAddingFileFailed', item, filter, options);
		};
		uploader.onAfterAddingFile = function(fileItem) {
		  // console.info('onAfterAddingFile', fileItem);
		};
		uploader.onAfterAddingAll = function(addedFileItems) {
		  // console.info('onAfterAddingAll', addedFileItems);
		};
		uploader.onBeforeUploadItem = function(item) {
				$scope.fileThumbnail = true;
		   //console.info('onBeforeUploadItem', item);
		};
		uploader.onProgressItem = function(fileItem, progress) {
		  // console.info('onProgressItem', fileItem, progress);
		};
		uploader.onProgressAll = function(progress) {
		  // console.info('onProgressAll', progress);
		};
		uploader.onSuccessItem = function(fileItem, response, status, headers) {
		  // console.info('onSuccessItem', fileItem, response, status, headers);
		};
		uploader.onErrorItem = function(fileItem, response, status, headers) {
		  // console.info('onErrorItem', fileItem, response, status, headers);
		};
		uploader.onCancelItem = function(fileItem, response, status, headers) {
		   //console.info('onCancelItem', fileItem, response, status, headers);
		   	var removeImg = MAIN.removeImg();
		   	removeImg.success(function(response) {
		   		if (response.status == 1) {
		   			$scope.fileThumbnail = false;
		   		}
		   	});
		};
		uploader.onCompleteItem = function(fileItem, response, status, headers) {
		  // console.info('onCompleteItem', fileItem, response, status, headers);

		};
		uploader.onCompleteAll = function(response) {

		};
   }
   /**
    * delete chat for the current user
    * @param  {[type]} stream [description]
    * @return {[type]}        [description]
    */
   $scope.deleteStream = function(stream) {
  	swal({
	  title: 'Do you want to remove this chat ?',
	  type: 'question',
	  showCancelButton: true,
	  confirmButtonColor: '#193246',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, Remove it!',
	  cancelButtonText: 'No, cancel!',
	  confirmButtonClass: 'btn',
	  cancelButtonClass: 'btn',
	  buttonsStyling: true
	}).then(function () {
	  	var deleteStream = MAIN.deleteStream({group_id: stream});
   		deleteStream.success(function(response) {
   			if (response.status == 1) {
   				$('#inbox'+stream).fadeOut();
   			}
   		});
	}, function (dismiss) {
	  
	});
   }


   $scope.moveToInbox = function() {
   	var toInbox = MAIN.toInbox({group_id: $routeParams.group_id});
   	toInbox.success(function(response) {
   		if (response.status == 1) {
   			window.history.back();
   		}
   	});
   }

   
   

});