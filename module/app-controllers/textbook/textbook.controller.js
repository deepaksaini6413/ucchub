(function () {
    'use strict';

    angular
        .module('app')
        .controller('TextBookController', TextBookController);

    TextBookController.$inject = ['UserService', 'TextBookService', '$rootScope', '$routeParams', '$scope','ngToast','$location'];
    function TextBookController(UserService, TextBookService, $rootScope, $routeParams, $scope,ngToast,$location) {
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.isProccessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.filteredTodos = [];
        $scope.currentPage = 1;
        $scope.numPerPage = 10;
        $scope.maxSize = 5;

        initController();

        function initController() {
              
            $scope.loadingImage=$rootScope.loadingImage;
            $rootScope.isUserLoggedIn=true;
            $scope.changeTextBook ="Send A Message";
            var slug = $routeParams.slug;
            if(slug == undefined){
                loadGetAllTextBooks();
            }else{
                LoadSingleTextBook(slug);
                customeBroughtTextBooks(slug);
                searchTextBookData(slug);
            }
            cateAndSubcatList();
        }

        $scope.sendAMessage = function(item){
            $scope.isProccessing = true;
            var bookid = 1;
            TextBookService.sendAMessage(bookid).then(function(res){
                $scope.isProccessing = true;
                if(res.status==200){
                    ngToast.create('Message sent successfully');
                    $scope.changeTextBook="Message sent";
                }
            });
        };

        $scope.getDataOnSubcat = function(catid,catname){
            $scope.selectedCtaName=catname;
            $scope.isProccessing=true;
            $scope.items=[];
            TextBookService.GetBooks(catid).then(function (response){
                var res = response;
                $scope.isProccessing=true;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                    $scope.items = res.data.Data;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }


        $scope.seeBookProfile = function(book_id){
            if(book_id){
                //window.location.href='#!/textBooks/'+book_id;
                $location.path('textBooks/'+book_id);
            }
            else{
              ngToast.create('Sorry No data found.');
            }
            
        }
         function cateAndSubcatList(){
             $scope.isProccessing=true;
            TextBookService.cateAndSubcatList().then(function (response){
                var res = response;
                $scope.itemList=[];
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.itemList = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                if(res.status != 200){
                    ngToast.create('Something went wrong');
                }
            });
         }

        $scope.searchTextBookById =function(category_id){
            $scope.isProccessing=true;
            TextBookService.searchTextBookById(category_id).then(function (response){
                var res = response;
                $scope.items=[];
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.items = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                
                if(res.status != 200){
                    ngToast.create('Something went wrong');
                }
            });
        }

        function customeBroughtTextBooks(){
            $scope.isProccessing=true;
            TextBookService.customeBroughtTextBooks($scope.lastpage).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.boughtitems = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                
                if(res.status != 200){
                    ngToast.create('Something went wrong');
                }
            });
        }

        function loadGetAllTextBooks()
        {
            $scope.isProccessing=true;
            TextBookService.GetBooks($scope.lastpage).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    // var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    // , end = begin + $scope.numPerPage;
                    // $scope.items = $scope.items.slice(begin, end);
        
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

        function LoadSingleTextBook(slug){
            $scope.isProccessing=true;
            TextBookService.GetBook(slug).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                        $scope.isDataFound=true;
                        $scope.items = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                    
                }
                if(res.status != 200){
                    ngToast.create('Something went wrong');
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $scope.isProccessing=true;
                TextBookService.loadMoreTextBooks($scope.lastpage).then(function (response){
                    var res = response;
                    $scope.isProccessing=false;
                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.isDataFound=true;
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                        $scope.totalTextBook=$scope.items.length;
                    }
                    
                    if(res.status == 0){
                        ngToast.create("Something went wrong");
                    }
                });
            }

        function  searchTextBookData(searchText){
            var slug = $routeParams.slug;
            $scope.searchText=slug;
           TextBookService.searchTextBook($scope.searchText).then(function(response){
                    if(response.status==200){
                        $scope.isProccessing=false;
                        if(response.data){
                          $scope.isDataFound=true;
                          $scope.searchTextBook=response.data.Data;
                          $scope.totalTextBook=$scope.searchTextBook.length;
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totalCollege=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.totalCollege=0;
                        $scope.searchText='';
                        $scope.isProccessing=false;
                        ngToast.create('cant load data from server');
                    }
                });
        }

         $scope.searchCollege = function(searchText){
            if($scope.searchText){
            $scope.isProccessing=true;
                TextBookService.searchTextBook($scope.searchText).then(function(response){
                    if(response.status==200){
                        //$scope.isProccessing=false;
                        if(response.data){
                          $scope.isDataFound=true;
                          $scope.searchTextBook=response.data.Data;
                          $scope.totalTextBook=$scope.searchTextBook.length;
                          if($scope.totalTextBook){
                            //window.location.href='#!/textBooks/search/'+$scope.searchText;
                            $location.path('textBooks/search/'+$scope.searchText);
                          }
                          $scope.searchText='';
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totalCollege=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.totalCollege=0;
                        $scope.searchText='';
                        $scope.isProccessing=false;
                        ngToast.create('cant load data from server');
                    }
                });
            }
            else{
                ngToast.dismiss();
                ngToast.create('Please Enter The Search Text');
            }
        } 

// $scope.makeTodos = function() {
//     $scope.todos = [];
//     for (var i=1;i<=1000;i++) {
//       $scope.todos.push({ book_id:i, book_title:'fdf',book_sub_title:'dsdsdh'});
//     }
//   };
//   $scope.makeTodos(); 

        $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
        , end = begin + $scope.numPerPage;
        $scope.items = $scope.items.slice(begin, end);
      });

    }

})();