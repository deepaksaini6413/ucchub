(function () {
    'use strict';

    angular
        .module('app')
        .controller('NewsController', NewsController);

    NewsController.$inject = ['NewsService', '$rootScope', '$routeParams','$scope', '$location'];
    function NewsController(NewsService, $rootScope, $routeParams, $scope, $location) {
        var vm = this;
        $scope.is_disable = false;
        vm.user = null;
        vm.allUsers = [];
        vm.items = null;
        $scope.items = [];  
        $scope.lastpage = 1;

        initController();

        function initController() {
            
            var slug = $routeParams.slug;
            if(slug == undefined){
                // loadNews();
                loadNewsData();
            }else{
                console.log('here')
                LoadSinglePage(slug);
            }
        }

        function loadNewsData() {
            NewsService.getNews().then(function(response) {
                var data = response.data;
                if (data.status == 1) {
                    $scope.featuredNews = data.featured_news;
                    $scope.newsCategories = data.categories;
                    $scope.currentNews = data.news.data;
                    $scope.nextUrl = data.news.next_page_url;
                }
            });    
        }

        function loadNews() {

            if(angular.equals({}, $rootScope.globals.currentUser)){
                $scope.user = $rootScope.globals.currentUser.username;
            }
            
            NewsService.News($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result.data;
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }
        
        function LoadSinglePage(slug){
            
            NewsService.singleNews(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                console.log(res);    
            });
        }

        $scope.loadMore = function() {
                $scope.lastpage +=1;

                NewsService.News($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
            }


        $scope.getCategoryNews = function(slug) {
            $scope.lastpage = 1;
            $scope.currentNews = [];
            NewsService.getNewsByCategory(slug).then(function(response) {
                var data = response.data;
                $scope.currentNews = data.news.data;
                $scope.newsCategory = data.category;
                $scope.nextUrl = data.news.next_page_url;
            });
        }

        $scope.loadMoreNews = function(url) {
            
            var url = url ? url : $rootScope.apiUrl+'/get-category-news/all?page=2';
            console.log(url);
            if(url){
                $scope.loading = true;
                NewsService.loadMoreData(url).then(function(response) {
                    $scope.loading = false;
                    var data = response.data.news.data;
                    console.log('data', data);
                    try {
                        angular.forEach(data, function(value, key){
                            console.log('value', value)
                            $scope.currentNews.push(value);
                        });
                    } catch (error) {
                        console.error(error)
                    }

                    $scope.currentNews = $scope.currentNews.push(data.news.data);
                    $scope.newsCategory = data.category;
                    $scope.nextUrl = data.news.next_page_url;
                });
        }
    }

})();