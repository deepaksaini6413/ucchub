(function () {
    'use strict';

    angular
        .module('app')
        .controller('NewsController', NewsController);

    NewsController.$inject = ['NewsService', '$cookies', '$rootScope', '$routeParams','$scope'];
    function NewsController(NewsService, $cookies, $rootScope, $routeParams, $scope) {
        var vm = this;
        $scope.is_disable = false;
        vm.user = null;
        vm.allUsers = [];
        vm.items = null;
        $scope.items = [];  
        $scope.lastpage = 1;
        $scope.isProccessing=false;
        $scope.loadingImage='';


        initController();

        function initController() {
            $scope.loadingImage="././images/loader.gif";
            var slug = $routeParams.slug;
            if(slug == undefined){
                // loadNews();
                loadNewsData();
            }else{
                LoadSinglePage(slug);
            }
        }

        function loadNewsData() {
            $scope.isProccessing=true;
            NewsService.getNews().then(function(response) {
                var data = response.data;
                $scope.isProccessing=false;
                if (data.status == 1) {
                    $scope.isDataFound = true;
                    $scope.breakingNews = data.featured_news;
                    $scope.newsCategories = data.categories;
                    $scope.currentNews = data.news.data;
                    $scope.nextUrl = data.news.next_page_url;
                }
            });    
        }

        function loadNews() {

            if(angular.equals({}, $rootScope.globals.currentUser)){
                $scope.user = $rootScope.globals.currentUser.username;
            }
            
            NewsService.News($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result.data;
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }
        
        function LoadSinglePage(slug){
            
            NewsService.singleNews(slug).then(function (response){
                var res = response;
                
                if(res.data.status == 1){
                    $scope.item = res.data.news;
                    $scope.related_news = res.data.related_news;
                    $scope.stats = res.data.stats;
                }
            });
        }

        $scope.newsActivity = function(id,action){


            var newsObj = $cookies.getObject('newsactivity');

            if(newsObj != undefined){
                
                if(newsObj.indexOf(id) !== -1) {

                    alert("you already performed action");
                    return false;

                }else{
                    newsObj.push(id);
                    $cookies.putObject('newsactivity',newsObj);
                }

            }else{
                
                $cookies.putObject('newsactivity',[id]);
            }

            NewsService.newsActivity(id,action).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.stats = res.data.stats;
                }
                  
            });
        }

        $scope.loadMore = function() {
                $scope.lastpage +=1;

                NewsService.News($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
            }


        $scope.getCategoryNews = function(slug) {
            $scope.lastpage = 1;
            $scope.currentNews = [];
            $scope.isProccessing=true;
            NewsService.getNewsByCategory(slug).then(function(response) {
                var data = response.data;
                $scope.isProccessing=false;
                $scope.currentNews = data.news.data;
                $scope.newsCategory = data.category;
                if(data.news.data.length > 0){
                    $scope.isDataFound = true;
                }
                else{
                    $scope.isDataFound = false;
                }
                $scope.nextUrl = data.news.next_page_url;
            });
        }


        $scope.loadMoreNews = function() {
                
                console.log("1")
                $scope.lastpage +=1;

                $scope.loading = true;
                NewsService.loadMoreData($scope.lastpage).then(function(response) {
                    
                    var data = response.data;
                    $scope.loading = false;
                    if(data.status == 1){
                            
                        if(data.news.current_page == data.news.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        angular.forEach(data.news.data, function(value, key){
                            $scope.currentNews.push(value);
                        });
                    }

                    if(data.status == 0){
                        alert("Something went wrong");
                    }
                });
            }
      }
})();
