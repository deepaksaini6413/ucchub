(function () {
    'use strict';

    angular
        .module('app')
        .controller('CollegeController', CollegeController);

    CollegeController.$inject = ['UserService', 'CollegeService', '$routeParams', '$rootScope','$scope','ngToast','$location','PagerService','NewsService','TextBookService'];
    function CollegeController(UserService, CollegeService, $routeParams, $rootScope, $scope,ngToast,$location,PagerService,NewsService,TextBookService) {
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.sortAplhaVar = "true";
        $scope.leftCategoryList=[];
        $scope.sortAplhabates=false;
        $scope.collegeData=[];
        $scope.totalCollege=0;
        $scope.isProccessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.currentPage = 1;
        $scope.numPerPage = 2;
        $scope.maxSize = 5;
        $scope.newItems=[];
        $scope.counter=0;

        initController();

         //$scope.dummyItems = _.range(1, 151); // dummy array of items to be paged
        vm.pager = {};
        vm.setPage = setPage;

        function setPage(page) {
            if (page < 1 || page > 10) {
                return;
            }
            vm.pager = PagerService.GetPager(20, page,vm.maxSize);
     
            // get current page of items
            $scope.newItems = $scope.newItems.slice(vm.pager.startIndex, vm.pager.endIndex + 1);
        }

        function initController() {
            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
            var slug = $routeParams.slug;
            if(slug == undefined){
                loadColleges();
            }else{
                LoadSinglePage(slug);
                loadNewsData();
                loadGetAllTextBooks();
                loadTopScholarship();
            }
            //call for getting left side list
            $scope.loadingImage=$rootScope.loadingImage;
            leftSideCollegeNavigation();
            bestCollegesData();
            setPage(1);
            loadTopScholarship();
        }
        // $scope.selectPage =function(pageNumber){
        //     //call api and send page number to it
        // }
        function showModal() {
          getLocation();
        }

        function getLocation() {
        if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
        } else {
        ngToast.create("Geolocation is not supported by this browser.");
        }
        }

        function loadNewsData() {
            $scope.isProccessing=true;
            NewsService.getNews().then(function(response) {
                var data = response.data;
                $scope.isProccessing=false;
                if (data.status == 1) {
                    $scope.isDataFound = true;
                    $scope.breakingNews = data.featured_news;
                    $scope.newsCategories = data.categories;
                    $scope.currentNews = data.news.data;
                    $scope.nextUrl = data.news.next_page_url;
                }
            });    
        }

        //for recent textbook added here:
        function loadGetAllTextBooks()
        {
            $scope.isProccessing=true;
            TextBookService.GetBooks($scope.lastpage).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    // var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    // , end = begin + $scope.numPerPage;
                    // $scope.items = $scope.items.slice(begin, end);
        
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

        // for top scholarship 
        function loadTopScholarship(){
            $scope.isProccessing=true;
            ScholarshipService.loadTopScholarship().then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.topScholarship = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        function showPosition(position) {
          ngToast.dismiss();
          $scope.lat=position.coords.latitude;
          $scope.lng=position.coords.longitude;
          var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    var geocoder = geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                $scope.currentAddress=results[1].formatted_address;
                                $('#myModal').modal();
                                $scope.$apply();
                                //ngToast.create("Location: " + results[1].formatted_address);
                            }
                        }
                        else{
                            ngToast.create("Location:Not Found");
                        }
                    });
        }

        function showError(error) {
        switch(error.code) {
        case error.PERMISSION_DENIED:
          x.innerHTML = "User denied the request for Geolocation."
          break;
        case error.POSITION_UNAVAILABLE:
          x.innerHTML = "Location information is unavailable."
          break;
        case error.TIMEOUT:
          x.innerHTML = "The request to get user location timed out."
          break;
        case error.UNKNOWN_ERROR:
          x.innerHTML = "An unknown error occurred."
          break;
        }
        }


        $scope.searchSearchLocation = function(){
            $scope.isProccessing=true;
            var data={
                "lat":$scope.lat,
                "lng":$scope.lng,
                "cureentAddress":$scope.currentAddress
            };

            CollegeService.searchCollegeByLocation(data).then(function (response){
                if(response.status==200){
                    $scope.isProccessing=false;
                    if(response.data){
                      $scope.collegeData=response.data.Data;
                      $scope.totalCollege=$scope.collegeData.length;
                    }
                    else{
                        ngToast.dismiss();
                        ngToast.create('Data Not found!.');    
                    }
                }
                else{
                    $scope.isProccessing=false;  
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        $scope.onKeySearch =function(text){
            if(text){}
            else{
                loadColleges();
            }    
        }

        $scope.seeCollegeByCategory = function(catId){
            showModal();
            //$scope.isProccessing=true;
            // CollegeService.seeCollegeByCategory(catId).then(function (response){
            //     if(response.status==200){
            //         $scope.isProccessing=false;
            //         if(response.data){
            //           $scope.collegeData=response.data.Data;
            //           $scope.totalCollege=$scope.collegeData.length;
            //         }
            //         else{
            //             ngToast.dismiss();
            //             ngToast.create('Data Not found!.');    
            //         }
            //     }
            //     else{
            //         $scope.isProccessing=false;
            //         ngToast.dismiss();
            //         ngToast.create('cant load data from server');
            //     }
            // });
        }

        function leftSideCollegeNavigation(){
            $scope.isProccessing=true;
            CollegeService.leftNavigationColleges().then(function (response){
                if(response.status==200){
                    $scope.isProccessing=false;
                    if(response.data){
                      $scope.leftCategoryList=response.data.Data;
                    }
                }
                else{
                    $scope.isProccessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        function loadColleges()
        {
            $scope.counter++;
            $scope.isProccessing=true;
            $scope.items=[];
            $scope.newItems=[];
            CollegeService.Colleges($scope.lastpage).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.result.length;
                    setrating($scope.items);
                    $scope.isDataFound = true;
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $scope.isProccessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
                // $scope.items=[];
                // $scope.newItems=[];
                // CollegeService.beseColleges().then(function(response){
                // if(response.status==200){
                //     $scope.isProccessing=false;
                //     if(response.data){
                //       $scope.items=response.data.Data;
                //       setrating($scope.items);
                //       $scope.isDataFound=true;
                //       //$scope.totalCollege=$scope.collegeData.length;
                //     }
                // }
                // else{
                //     $scope.isDataFound=false;
                //     $scope.isProccessing=false;
                //     ngToast.dismiss();
                //     ngToast.create('cant load data from server');
                // }
            //});
        }

        function setrating(data){
            $scope.newItems=[];
            for(var i=0;i<data.length;i++){
                $scope.newItems.push(
                {
                  "Id": $scope.items[i].id,
                  "title": $scope.items[i].title,
                  "college_rank": $scope.items[i].college_rank,
                  "college_image": $scope.items[i].college_image,
                  "address":$scope.items[i].address,
                  "time_period":$scope.items[i].time_period,
                  "no_of_students":$scope.items[i].no_of_students,
                  "net_price":$scope.items[i].net_price,
                  "rating":$scope.items[i].rating,
                  "votes":$scope.items[i].votes,
                  "reviews":$scope.items[i].reviews,
                  "slug":$scope.items[i].slug,
                  "className": ($scope.items[i].rating < 2 ? "bg_red" : (($scope.items[i].rating >= 2 && $scope.items[i].rating <= 3.5) ? "bg_yellow" : "bg_green"))
                });
            }

            $rootScope.saveSearchdata=$scope.newItems;
            if($scope.newItems.length > 0){
                //$scope.newItems=$scope.newItems.slice(0,5);
                //$scope.totalCollege=$scope.newItems.length;
            }else{

            }
        }

        function LoadSinglePage(slug){
            $scope.isProccessing=true;
            CollegeService.College(slug).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    $scope.item = res.data.result;
                    $scope.item.className = ($scope.item.rating < 2 ? "bg_red" : (($scope.item.rating >= 2 && $scope.item.rating <= 3.5) ? "bg_yellow" : "bg_green"));
                }
                console.log(res);    
            });
        }

        function loadTopScholarship(){
            $scope.isProccessing=true;
            CollegeService.loadTopScholarship().then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.topScholarship = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $scope.isProccessing=true;
                CollegeService.Colleges($scope.lastpage).then(function (response){
                    var res = response;
                    $scope.isProccessing=false;
                    if(res.data.status == 1){
                        if(res.data.current_page == res.data.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        $scope.totalCollege=res.data.to;
                        $scope.newItems = $scope.newItems.concat(res.data.result);
                    }
                    if(res.status == 0){
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
             // $scope.newItems=$rootScope.saveSearchdata;
             // $scope.totalCollege=$scope.newItems.length;
             //$scope.newItems=$rootScope.saveSearchdata.slice(5,$rootScope.saveSearchdata.length -1);
        }

        $scope.addToWishList = function(item_id)
        {
            if(!$rootScope.isUserLoggedIn)
            {
                ngToast.create("Please login to add this school into your wishlist");
            }

            if($rootScope.isUserLoggedIn)
            {
                CollegeService.AddToWishList(item_id).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        // To Do task    
                    }
                    
                    if(res.status == 0){
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
            }
        }
        $scope.sortAplha = function()
        {
            $scope.newItems=[];
            $scope.sortAplhabates=true;
            $scope.sortType = 'title';
            $scope.isProccessing=true;
            CollegeService.sortCollegeByAlphabates($scope.lastpage).then(function (response){
                var res = response;
                $scope.isProccessing=true;
                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.result.length;
                    setrating($scope.items);
                    $scope.isDataFound = true;
                    console.log($scope.items);
                }
                else{
                    $scope.isProccessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $scope.isProccessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }
        $scope.bestCollege= function (){
            //bestCollegesData();
            loadColleges();
        }

         function bestCollegesData(){
            $scope.collegeData=[];
            $scope.sortAplhabates=true;
            $scope.isProccessing=true;
            CollegeService.beseColleges().then(function(response){
                if(response.status==200){
                    $scope.isProccessing=false;
                    if(response.data){
                      $scope.collegeData=response.data.Data;
                      //$scope.totalCollege=$scope.collegeData.length;
                    }
                }
                else{
                    $scope.isProccessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        $scope.searchCollege = function(){
            if($scope.searchText){
                $scope.collegeData=[];
                $scope.sortAplhabates=true;
                $scope.isProccessing=true;
                // CollegeService.searchColleges($scope.searchText).then(function(response){
                //     $scope.isProccessing=false;
                //     if(response.status==200){
                //         if(response.data){
                //           $scope.searchText='';
                //           $scope.isDataFound=true;
                //           $scope.collegeData=response.data.Data;
                //           $scope.totalCollege=$scope.collegeData.length;
                //         }
                //         else{
                //             $scope.isDataFound=false;
                //             $scope.totalCollege=0;
                //             $scope.searchText='';
                //             $scope.isProccessing=false;
                //         }
                //     }
                //     else{
                //         $scope.totalCollege=0;
                //         $scope.searchText='';
                //         $scope.isProccessing=false;
                //         ngToast.dismiss();
                //         ngToast.create('cant load data from server');
                //     }
                // });
                 $location.path('/colleges'); 
                $scope.newItems=[];
                for(var i=0;i<$rootScope.saveSearchdata.length;i++){
                    if(($rootScope.saveSearchdata[i].title.toLowerCase()).indexOf($scope.searchText.toLowerCase()) !== -1){
                        $scope.newItems.push($rootScope.saveSearchdata[i]);
                    }
                }
                $scope.totalCollege=$scope.newItems.length;
                if($scope.totalCollege){
                    $scope.isDataFound=true;
                }
                    else{
                        $scope.isDataFound=false;
                    }
            }
            else{
                ngToast.dismiss();
                ngToast.create('Please Enter The Search Text');
            }
        }

       $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
        , end = begin + $scope.numPerPage;
        //l$scope.totalCollege=$scope.newItems.length;
        $scope.newItems = $scope.items.slice(begin, end);
    
      });    

}
})();