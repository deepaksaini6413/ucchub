(function () {
    'use strict';

    angular
        .module('app')
        .controller('JobController', JobController);

    JobController.$inject = ['UserService', 'JobService', '$routeParams', '$rootScope','$scope','ngToast'];
    function JobController(UserService, JobService, $routeParams, $rootScope, $scope,ngToast) {
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.Experiance=[];
        $scope.salary=[];
        $scope.loadingImage='';
        $scope.currentPage = 1;
        $scope.jobDescrption=[];
        $scope.numPerPage = 3;
        $scope.isDataFound=true;
        $scope.maxSize = 5;
        initController();

        function getExperience(){
            $scope.Experiance.push(
                {key:1,value:1},
                {key:2,value:2},
                {key:3,value:3},
                {key:4,value:4},
                {key:5,value:5},
                {key:6,value:6},
                {key:7,value:7},
                {key:8,value:8},
                {key:9,value:9},
                {key:10,value:10}
                );

            $scope.salary.push(
                {key:1000,value:1000},
                {key:2000,value:2000},
                {key:3000,value:3000},
                {key:4000,value:4000},
                {key:5000,value:5000},
                {key:6000,value:6000},
                {key:7000,value:7000},
                {key:8000,value:8000},
                {key:9000,value:9000},
                {key:10000,value:10000}
                );

            //$scope.states.push({id:1,name:'Delhi'},{id:2,name:'Mumbai'},{id:3,name:'Surat'});
            // $scope.locations.push(
            //     {id:1,name:'Mumbai'},
            //     {id:2,name: 'Delhi'},
            //     {id:3,name: 'Punjab'},
            //     {id:4,name: 'Gurgaon'});
            //$scope.search.experience=$scope.Experiance[1].key;
        }

        function initController() {
            $scope.states=[];
            $scope.Experiance=[];
            $scope.locations=[];
            $scope.loadingImage=$rootScope.loaderImageUrl;
            var slug = $routeParams.slug;
            if(slug == undefined){
                loadJobs();
            }else{
                //LoadSinglePage(slug);
                //jobDescriptionByID(slug);
            }
            $rootScope.isUserLoggedIn=true;
            getExperience();

            var jobId = $routeParams.id;
            if(jobId){
                jobDescriptionBySlug(jobId);
            }
        }

        $scope.addToFavouriteJob = function(event,jobId){
            $scope.jobAddToFavorite(jobId);
            // if(jobId){
            //     if(angular.element(event.target).hasClass('favoriteJob')){
            //         angular.element(event.target).removeClass('favoriteJob');
            //     }
            //     else{
            //      ngToast.dismiss();   
            //      ngToast.create('Job added into Favourite List.');   
            //      angular.element(event.target).addClass('favoriteJob');   
            //     }
            // }
        }

        $scope.searchFreshness = function(event){
            if($scope.freshness){
                if(event.which==13 || event.keyCode==13){
                    //api data calling here
                    $scope.globalJobSearch($scope.freshness,'',0,0);
                    //$scope.isProccessing=true;
                    ngToast.dismiss();
                    ngToast.create('searched content');
                }
            }
            else{
                $scope.isProccessing=false;
                ngToast.dismiss();
                ngToast.create('Enter Freshness text here.');
            }
        }

        $scope.searchClickOnLocation = function(event,search){
            $scope.data=[];
            var totalLocation = document.getElementsByClassName('isCheck');
            for(var i=0;i<totalLocation.length;i++){
                if(totalLocation[i].checked){
                    $scope.data.push(totalLocation[i].value);
                }
            }
            console.log($scope.data+" bnkmkmnbm "+event.currentTarget.value);
            if($scope.data.length > 0){}
            else{
                $scope.items=$scope.savedJobs;
            }    

          if($scope.data.length){
            $scope.isProccessing=true;
            if($scope.search){
                var data={
                searchText:$scope.search.free_search,
                location:$scope.search.location,
                selectedExperience:$scope.search.experience,
                selectedSalary :$scope.search.salary
                };
            }
            else{
                var data={
                searchText:'',
                location:'',
                selectedExperience:0,
                selectedSalary :0
            };
            }
            
             JobService.searchJobsBasedOnLocation($scope.data,data).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                console.log(res);
                if(res.data.status == 1){
                    if(res.data.Data.length > 0){
                        $scope.items = res.data.jobs.data;
                        $scope.states = res.data.states;
                        $scope.isDataFound=true;
                    if(res.data.jobs.current_page == res.data.jobs.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    }
                    else{
                        ngToast.dismiss();
                        $scope.isDataFound=false;
                        $scope.items=[];
                        ngToast.create("Oop's No Job Found!.");
                    }
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });   
          }
        }

        function loadJobs()
        {
            $scope.isProccessing=true;
            JobService.Jobs($scope.lastpage).then(function (response){
                var res = response;
                console.log(res);
                $scope.isProccessing=false;
                if(res.data.status == 1){
                    if(res.data.jobs.current_page == res.data.jobs.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    if(res.data.jobs.data.length > 0){
                      $scope.items = res.data.jobs.data;
                      $scope.totaljobLanding=$scope.items.length;
                      $scope.savedJobs=$scope.items;
                    }
                    else{
                        ngToast.create('No jobs Found');
                    }

                    if(res.data.states.length > 0){
                      $scope.locations= res.data.states;
                     $scope.states=  res.data.states;
                        // for(var i=0;i<res.data.states.length;i++){
                        //   $scope.states.push({key:res.data.states[i].name,value:res.data.states[i].name});      
                        // }
                    }
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    $scope.isProccessing=false;
                    ngToast.create("Something went wrong");
                }
            });
            // $scope.isProccessing=true;
            // JobService.Jobs().then(function (response){
            //     if(response.status==200){
            //         $scope.isProccessing=false;
            //         if(response.data){
            //           $scope.items=response.data.Data;
            //           $scope.savedJobs=$scope.items;
            //           $scope.items=$scope.savedJobs.slice(0,5);
            //           $scope.totaljobLanding=$scope.jobLandingData.length;
            //         }
            //     }
            //     else{
            //         $scope.isProccessing=false;
            //         window.alert('cant load data from server');
            //     }
            // });
        }


        function jobDescriptionBySlug(jobSlug)
        {
            $scope.isProccessing=true;
            JobService.jobDescriptionBySlug(jobSlug).then(function (response){
                if(response.status==200){
                    $scope.isProccessing=false;
                    if(response.data){
                      $scope.jobDescrption=response.data.Data;
                    }
                }
                else{
                    $scope.isProccessing=false;
                    window.alert('cant load data from server');
                }
            });
        }

        $scope.searchJob = function(searchData){
            $scope.globalJobSearch(searchData.free_search,searchData.location,searchData.experience,searchData.salary);
            // JobService.searchJob($scope.search).then(function(res) {
            //     if (res.data.status == 1) {
            //         if (res.data.jobs.current_page == res.data.jobs.last_page) {
            //             $scope.is_disable = true;
            //         }
            //         $scope.items = res.data.jobs.data;
            //         $scope.states = res.data.states;
            //     }
            // });
        }

        // function LoadSinglePage(slug){
            
        //     JobService.Job(slug).then(function (response){
        //         var res = response;
        //         if(res.data.status == 1){
        //             $scope.item = res.data.result;
        //         }
        //         console.log(res);    
        //     });
        // }

        $scope.loadMore = function() 
        {
             // console.log('loadMore');
             //    $scope.lastpage +=1;

             //    JobService.Jobs($scope.lastpage).then(function (response){
             //        var res = response;

             //        if(res.data.status == 1){
                        
             //            if(res.data.result.current_page == res.data.result.last_page)
             //            {
             //                $scope.is_disable = true;
             //            }

             //            $scope.items = $scope.items.concat(res.data.result.data);
             //        }
                    
             //        if(res.status == 0){
             //            alert("Something went wrong");
             //        }
             //    });
             $scope.items=$scope.savedJobs;
             $scope.totaljobLanding=$scope.items.length;
        }

        $scope.jobApply =function(jobId){
            //job apply here
            var data={
                userid :1,
                jobid:jobId
            };

            $scope.isProccessing=true;
            JobService.jobApply(data).then(function (response){
                ngToast.dismiss();
                if(response.data.status==1){
                    $scope.isProccessing=false;
                    ngToast.create(response.data.message);
                }
                if(response.data.status==0){
                    $scope.isProccessing=false;
                    ngToast.create(response.data.message);
                }
                // else{
                //     $scope.isProccessing=false;
                //     ngToast.create('Data cant be load from server');
                // }
            });
        }

        $scope.jobAddToFavorite= function(jobId){
            $scope.isProccessing=true;
             var data={
                userid :1,
                jobid:jobId
            };

            if(jobId){
                    JobService.jobAddToFavorite(data).then(function (response){
                        ngToast.dismiss();
                        if(response.data.status==1){
                            $scope.isProccessing=false;
                            ngToast.create(response.message);
                        }
                        if(response.data.status==0){
                            $scope.isProccessing=false;
                            angular.element(event.target).addClass('favoriteJob'); 
                            ngToast.create(response.data.message);
                        }
                        // else{
                        //     $scope.isProccessing=false;
                        //     ngToast.create('Data cant be load from server');
                        // }
                    });

                if(angular.element(event.target).hasClass('favoriteJob')){
                    angular.element(event.target).removeClass('favoriteJob');
                }
                else{
                 ngToast.dismiss();   
                 //ngToast.create('Job added into Favourite List.');   
                 angular.element(event.target).addClass('favoriteJob');   
                }
            }
        }
        //job seeRecruiterNumber
        $scope.seeRecruiterNumber =function(jobId){
            $scope.isProccessing=true;
            $('#myModal').modal();
            JobService.seeRecruiterNumber(jobId).then(function (response){
                if(response.data.status==1){
                    $scope.isProccessing=false;
                    $scope.recruiter=response.data.recruiter;
                }
                else{
                    $scope.isProccessing=false;
                    ngToast.create('Data cant be load from server');
                }
                $scope.$apply();
            });

        }

        $scope.globalJobSearch = function(searchText,location,selectedExperience,selectedSalary){
            if(searchText||location||selectedExperience||selectedSalary){
                $scope.isProccessing=true;
                JobService.globalJobSearch(searchText,location,selectedExperience,selectedSalary)
                .then(function(response){
                    
                    if(response.data.status==1){
                        $scope.isProccessing=false;
                        if(response.data.jobs.data){
                          $scope.searchText='';
                          $scope.isDataFound=true;
                          $scope.items=response.data.jobs.data;
                          $scope.totaljobs=response.data.jobs.data.length;
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totaljobs=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.globalJobSearch=0;
                        $scope.searchText='';
                        $scope.isProccessing=false;
                        ngToast.create('cant load data from server');
                    }
                });
            }
            else{
                window.alert('Please Enter The Search Text');
            }
        }

        $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
        , end = begin + $scope.numPerPage;
        $scope.items = $scope.items.slice(begin, end);
      });

    };

})();