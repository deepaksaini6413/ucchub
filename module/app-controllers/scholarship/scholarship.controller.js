(function () {
    'use strict';

    angular
        .module('app')
        .controller('ScholarshipController', ScholarshipController);

    ScholarshipController.$inject = ['UserService', 'ScholarshipService', '$rootScope', '$routeParams', '$scope','ngToast','$location'];
    function ScholarshipController(UserService, ScholarshipService, $rootScope, $routeParams, $scope,ngToast,$location) {
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.isProccessing=false;
        $scope.isDataFound=false;

        initController();

        function initController() {
            $scope.loadingImage=$rootScope.loadingImage;
            var slug = $routeParams.slug;
            if(slug == undefined){
                loadScholarships();
            }else{
                LoadSinglePage(slug);
            }
            testimonialsTeam();
            loadTopScholarship();
        }

        $scope.UCCHUB = function(clickAction){

        }

        function loadTopScholarship(){
            $scope.isProccessing=true;
            ScholarshipService.loadTopScholarship().then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.topScholarship = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        $scope.startSearch = function(){
            $('#myModal').modal();
            // $scope.isProccessing=true;
            // ScholarshipService.startSearch().then(function(response){
            //     var res= response;
            //     $scope.isProccessing=false;
            //     if(res.status==200){
            //         if(res.data){
            //             //business logic here
            //         }
            //     }
            // });
        }

        $scope.collegeDictory = function(){
            $location.path('/colleges'); 
            // $scope.isProccessing=true;
            // ScholarshipService.collegeDictory().then(function(response){
            //     var res= response;
            //     $scope.isProccessing=false;
            //     if(res.status==200){
            //         if(res.data){
            //             //business logic here
            //         }
            //     }
            // });
        }

        $scope.matchMaker = function(){
            $scope.isProccessing=true;
            ScholarshipService.matchMaker().then(function(response){
                var res= response;
                $scope.isProccessing=false;
                if(res.status==200){
                    if(res.data){
                        //business logic here
                    }
                }
            });
        }

        function testimonialsTeam(){
            $scope.isProccessing=true;
            ScholarshipService.testimonialsTeam().then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.testimonialsTeam = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        function loadScholarships()
        {
            console.log("hello");
            ScholarshipService.Scholarships($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result.data;
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }

        function LoadSinglePage(slug){
            
            ScholarshipService.Scholarship(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                console.log(res);    
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;

                ScholarshipService.Scholarships($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
            }
    }

})();