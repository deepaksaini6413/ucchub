﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', '$rootScope','$routeParams','AuthenticationService', 'FlashService','$scope'];
    function LoginController($location, $rootScope, $routeParams, AuthenticationService, FlashService,$scope) {
        var vm = this;

        vm.login = login;

        var verification_code = $routeParams.verification_code;

        if(verification_code){

            var request = AuthenticationService.EmailVerfication(verification_code);

        }
        

        (function initController() {
            // reset login status
            console.log($rootScope.isUserLoggedIn);
            if($rootScope.isUserLoggedIn == true){
                $location.path('/');
            }else{
                 AuthenticationService.ClearCredentials();
                 $rootScope.isUserLoggedIn = false;       
            }

        })();

        function login() {
            vm.dataLoading = true;
            $scope.submitButton = true;
            AuthenticationService.Login(vm.email, vm.password, function (response) {
                if (response.success == 200) {

                    if(response.status == 1)
                    {
                        window.localStorage.setItem('user',JSON.stringify(response.data));
                        $scope.submitButton = false;
                        $rootScope.isUserLoggedIn = true;
                        console.log('loggedin');
                        var loggedinUsername = response.data.first_name + " " + response.data.last_name;
                        $rootScope.name = loggedinUsername;
                        AuthenticationService.SetCredentials(vm.email,response.token,loggedinUsername);
                        $location.path('/');
                    }

                    if(response.status == 0)
                    {
                        FlashService.Error(response.message);
                        $scope.submitButton = false;
                        vm.dataLoading = false;
                    }

                    
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };
    }

})();
