﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['UserService', 'BookService', '$rootScope','$scope','$location','$window'];
    function HomeController(UserService, BookService, $rootScope, $scope,$location,$window) {
        var vm = this;
        $scope.is_disable = false;
        vm.user = null;
        vm.allUsers = [];
        vm.deleteUser = deleteUser;
        vm.books = null;
        $scope.books = [];  
        $scope.lastpage = 1;

        initController();
        // function forceSSL() {
        //     if ($location.protocol() !== 'https') {
        //         $window.location.href = $location.absUrl().replace('http', 'https');
        //     }
        // };

        function initController() {
           //forceSSL();
            loadGetAllTextBooks();            
            loadCurrentUser();
            //loadAllUsers();
        }

        function loadGetAllTextBooks()
        {
            $scope.isProccessing=true;
            BookService.GetBooks($scope.lastpage).then(function (response){
                var res = response;
                $scope.isProccessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    // var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    // , end = begin + $scope.numPerPage;
                    // $scope.items = $scope.items.slice(begin, end);
        
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

        function loadCurrentUser() {

            if(angular.equals({}, $rootScope.globals.currentUser)){
                $scope.user = $rootScope.globals.currentUser.username;
            }
            
            BookService.Books($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.books = res.data.result.data;
                    console.log($scope.books);
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
            

            //return;
            // UserService.GetByUsername($rootScope.globals.currentUser.username)
            //     .then(function (user) {
            //         vm.user = user;
            //     });
        }

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $scope.isProccessing=true;
                BookService.GetBooks($scope.lastpage).then(function (response){
                    var res = response;
                    $scope.isProccessing=false;
                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.isDataFound=true;
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                        $scope.totalTextBook=$scope.items.length;
                    }
                    
                    if(res.status == 0){
                        ngToast.create("Something went wrong");
                    }
                });
        }

  // function loadExternalScript(scriptUrl) {
  //   return new Promise((resolve, reject) => {
  //     const scriptElement = document.createElement('script')
  //     scriptElement.src = scriptUrl
  //     scriptElement.onload = resolve
  //     document.body.appendChild(scriptElement)
  //   });
  // }

// $scope.paypalIntegration= function(){
//    loadExternalScript("https://www.paypalobjects.com/api/checkout.js").then(() => {
//       paypal.Button.render({
//         env: 'sandbox',
//         client: {
//           production: '',
//           sandbox: 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R'
//         },
//         commit: true,
//         payment: function (data, actions) {
//           return actions.payment.create({
//             payment: {
//               transactions: [
//                 {
//                   amount: { total: 20, currency: 'AUD' }
//                 }
//               ]
//             }
//           })
//         },
//         onAuthorize: (data, actions) => {
//           return actions.payment.execute().then((payment) => {
//             console.log(JSON.stringify(payment),"Payment");
//           })
//         }
//       }, '#paypal-button-container');
//     });
//   }

    }

})();