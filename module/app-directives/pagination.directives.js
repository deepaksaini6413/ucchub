(function (angular, undefined) {
    'use strict';

    angular
        .module('app')
        .directive('mySharedScope', function () {
		    return {
		        template: 'Name: Deepak Saini'
		    };
		});
});