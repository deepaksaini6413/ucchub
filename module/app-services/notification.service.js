(function () {
    'use strict';

    angular
        .module('app')
        .factory('NotificationService', NotificationService);

    NotificationService.$inject = ['$rootScope','$http'];
    function NotificationService($rootScope, $http) {
        var service = {};

        service.Notifications = Notifications;
        service.Scholarship = Scholarship;
        service.testimonialsTeam = testimonialsTeam;
        service.loadTopScholarship = loadTopScholarship;
        //initService();

        return service;

        // private functions

        function handleSuccess(res) {
            return res;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

        function loadTopScholarship(){
            return $http.get($rootScope.localApiUrl+'sampleData/topScholarship.json').
           then(handleSuccess, handleError('testimonialsTeam'));
        }
        
        function Notifications(lastpage) {
            return $http.get($rootScope.localApiUrl+'sampleData/notification.json').
           then(handleSuccess, handleError('testimonialsTeam'));
           //return $http.get($rootScope.apiUrl+'/get-module/scholarship?page='+lastpage).then(handleSuccess, handleError('Error getting all books'));
        }

        function Scholarship(slug) {

            return $http.get($rootScope.apiUrl+'/get-single-module/scholarship/'+slug).then(handleSuccess, handleError('Error getting single item'));
        }

        function testimonialsTeam(){
            return $http.get($rootScope.localApiUrl+'sampleData/testimonialsTeam.json').
           then(handleSuccess, handleError('testimonialsTeam'));
        }
        
    }

})();