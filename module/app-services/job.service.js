(function() {
    'use strict';

    angular
        .module('app')
        .factory('JobService', JobService);

    JobService.$inject = ['$rootScope', '$http'];

    function JobService($rootScope, $http) {
        var service = {};

        service.Jobs        = Jobs;
        service.Job         = Job;
        service.UserJobs    = UserJobs; 
        service.Create      = Create;
        service.Update      = Update;
        service.searchJob   = searchJob;
        service.jobDescriptionBySlug = jobDescriptionBySlug;
        service.jobDescriptionByID = jobDescriptionByID;
        service.globalJobSearch=globalJobSearch;
        service.searchJobsBasedOnLocation = searchJobsBasedOnLocation;
        service.loadMoreData = loadMoreData;
        service.jobApply =jobApply;
        service.jobAddToFavorite = jobAddToFavorite;
        service.seeRecruiterNumber = seeRecruiterNumber;
        //initService();

        return service;

        // private functions

        function handleSuccess(res) {
            return res;
        }

        function handleError(error) {
            return function() {
                return { success: false, message: error };
            };
        }
        function seeRecruiterNumber(jobId){
            var data={
                jobid:jobId
            };
         return $http.post($rootScope.apiUrl+'/get-recuiter-details',data).
         then(handleSuccess, handleError('Error getting all books'));
        }

        function Jobs(lastpage) {
           return $http.get($rootScope.apiUrl+'/get-module/jobs?page='+lastpage).
           then(handleSuccess, handleError('Error getting all books'));
           // return $http.get($rootScope.localApiUrl+'sampleData/jobLanding.json').
           // then(handleSuccess, handleError('job landing data'));
        }

        function jobApply(data){
            return $http.post($rootScope.apiUrl + '/apply-job', data).
            then(handleSuccess, handleError('Error getting all books'));
        }
         function jobAddToFavorite(data){
            return $http.post($rootScope.apiUrl + '/add-job-to-favourite', data).
            then(handleSuccess, handleError('Error getting all books'));
         }

        function jobDescriptionBySlug(slug){
            return $http.get($rootScope.apiUrl + '/job-description/' + slug).then(handleSuccess, handleError('Error getting all books'));
        }

        function jobDescriptionByID(jobId){
            return $http.get($rootScope.localApiUrl+'sampleData/jobDescription.json').
           then(handleSuccess, handleError('single job landing data'));
            //return $http.get($rootScope.apiUrl + '/get-module/jobs?page=' + lastpage).then(handleSuccess, handleError('Error getting all books'));
        }

        function Job(slug) {
            return $http.get($rootScope.apiUrl + '/get-single-module/jobs/' + slug).then(handleSuccess, handleError('Error getting single item'));
        }

        function Create(data) {
            return $http.post($rootScope.apiUrl + '/jobs/create', data).then(handleSuccess, handleError('Error getting single books'));
        }

        function UserJobs(lastpage) {
            return $http.get($rootScope.apiUrl + '/get-module/jobs?page=' + lastpage).then(handleSuccess, handleError('Error getting all books'));
        }

        function Update(data) {
            return $http.post($rootScope.apiUrl + '/jobs/' + data.id + '/update', data).then(handleSuccess, handleError('Error on updating Job'));
        }

        function searchJob(params) {
            return $http.post($rootScope.apiUrl + '/jobs/search', params).
            then(handleSuccess, handleError('Error getting single books'));
        }

        function loadMoreData(lastpage, search) {
            return $http.post($rootScope.apiUrl + '/get-jobs?page=' + lastpage, search).then(handleSuccess, handleError('Error getting loadmmore news'));
        }

        function globalJobSearch(searchText,location,selectedExperience,selectedSalary) {
            var data={
                location:location,
                searchText:searchText,
                selectedSalary:selectedSalary,
                selectedExperience:selectedExperience
            };
            return $http.post($rootScope.apiUrl + '/jobs/search', {data:data}).then(handleSuccess, handleError('Error getting single books'));            
        }

        function searchJobsBasedOnLocation(locations,data){
            var searchText= data.searchText ? data.searchText :'';
            var location= data.location ? data.location :'';
            var selectedExperience= data.selectedExperience ? data.selectedExperience :0;
            var selectedSalary= data.selectedSalary ? data.selectedSalary :0;
         return $http.post($rootScope.apiUrl + '/job-on-location', {data:locations,searchText:searchText,location:location,selectedExperience:selectedExperience,selectedSalary:selectedSalary}).then(handleSuccess, handleError('Error getting single books'));               
        } 
    }

})();