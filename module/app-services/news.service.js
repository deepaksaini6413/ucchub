(function () {
    'use strict';

    angular
        .module('app')
        .factory('NewsService', NewsService);

    NewsService.$inject = ['$rootScope','$http'];
    function NewsService($rootScope, $http) {
        var service = {};

        service.News = News;
        service.singleNews = singleNews;
        service.getNews = getNews;
        service.getNewsByCategory = getNewsByCategory;
        service.loadMoreData = loadMoreData;
        service.newsActivity = newsActivity;
        //initService();

        return service;

        // private functions

        function handleSuccess(res) {
            return res;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

        function News(lastpage) {
           return $http.get($rootScope.apiUrl+'/get-module/news?page='+lastpage).then(handleSuccess, handleError('Error getting all news'));
        }

        function singleNews(slug) {
            return $http.get($rootScope.apiUrl+'/get-single-module/news/'+slug).then(handleSuccess, handleError('Error getting single item'));
        }

        function getNews() {
            return $http.get($rootScope.apiUrl+'/get-news').then(handleSuccess, handleError('Error getting single item'));
        }

        function getNewsByCategory(slug) {
            return $http.get($rootScope.apiUrl+'/get-category-news/'+slug).then(handleSuccess, handleError('Error getting single item'));
        }
        function loadMoreData(lastpage) {
            return $http.get($rootScope.apiUrl+'/get-news?page='+lastpage).then(handleSuccess, handleError('Error getting loadmmore news'));
        }
        function newsActivity(id,action){
            return $http.get($rootScope.apiUrl+'/news-activity/'+id+'/'+action).then(handleSuccess, handleError('Error do news activity'));
        }
    }

})();