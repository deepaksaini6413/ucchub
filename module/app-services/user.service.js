﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http','$rootScope'];
    function UserService($http,$rootScope) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.GetState = GetState;
        service.GetEducationLevel = GetEducationLevel;
        service.GetStateEducationlevelCollege = GetStateEducationlevelCollege;
        service.ForgetPasswordRequest   = ForgetPasswordRequest;
        service.ResetPasswordRequest    = ResetPasswordRequest;
        service.Profile     =   Profile;
        service.updateInfo  =   updateInfo;

        // message module
        service.getInbox = getInbox;
        service.getContactUser = getContactUser;
        service.getGroup = getGroup;
        service.sendMessage = sendMessage;
        return service;

        function GetAll() {
            return $http.get($rootScope.apiUrl+'/users').then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.get($rootScope.apiUrl+'/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }

        function Profile(userId) {
            return $http.post($rootScope.apiUrl+'/get-profile-info',{userid:userId}).
            then(handleSuccess, handleError('Error getting current user profile'));
        }

        function updateInfo(data){
            return $http.post($rootScope.apiUrl+'/users',data).then(handleSuccess,handleError('Error while updating user information'));
        }

        function GetByUsername(username) {
            return $http.post($rootScope.apiUrl+'/auth/login',username).then(handleSuccess, handleError('Error creating user'));
            //return $http.get('/api/users/' + username).then(handleSuccess, handleError('Error getting user by username'));
        }

        function Create(user) {
            return $http.post($rootScope.apiUrl+'/auth/register', user).then(handleSuccess, handleError('Error creating user'));
        }

        function Update(user) {
            return $http.put($rootScope.apiUrl+'/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(id) {
            return $http.delete($rootScope.apiUrl+'/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            
            return function () {
                return { success: false, message: error };
            };
        }

        function GetState(){
            return $http.get($rootScope.apiUrl+'/get/state').then(handleSuccess, handleError('Error gettiing state'));
        }

        function GetEducationLevel(){
            return $http.get($rootScope.apiUrl+'/get/education-level').then(handleSuccess, handleError('Error gettiing education level'));
        }

        function GetStateEducationlevelCollege(){
            return $http.get($rootScope.apiUrl+'/get/signup-step-one').then(handleSuccess, handleError('Error gettiing state, education level and colleges'));
        }

        function ForgetPasswordRequest (user) {
            return $http.post($rootScope.apiUrl +'/password/reset', user).then(handleSuccess, handleError('Request Failed'));
        }

        function ResetPasswordRequest (user) {
            return $http.post($rootScope.apiUrl +'/password/reset', user).then(handleSuccess, handleError('Request Failed'));
        }

        function getInbox() {
            return $http.post($rootScope.apiUrl + '/get-inbox').then(handleSuccess, handleError('Request Failed'));
        }

        function getContactUser() {
            return $http.post($rootScope.apiUrl + '/get-contact-user').then(handleSuccess, handleError('Request Failed'));
        }

        function getGroup(userId) {
            return $http.post($rootScope.apiUrl + '/get-group', {user: userId}).then(handleSuccess, handleError('Request Failed'));
        }

        function sendMessage(data) {
            return $http.post($rootScope.apiUrl + '/send-message', data).then(handleSuccess, handleError('Request Failed'));
        }
    }

})();
