(function () {
    'use strict';

    angular
        .module('app')
        .factory('TextBookService', TextBookService);

    TextBookService.$inject = ['$rootScope','$http'];
    function TextBookService($rootScope, $http) {
        var service = {};

        service.GetBooks       = GetBooks;
        service.GetBook =         GetBook;
        service.customeBroughtTextBooks =customeBroughtTextBooks;
        service.searchTextBook =searchTextBook;
        service.cateAndSubcatList= cateAndSubcatList;
        service.loadMoreTextBooks = loadMoreTextBooks;
        service.sendAMessage = sendAMessage;
        //initService();

        return service;

        // private functions

        function handleSuccess(res) {
            return res;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

        function sendAMessage(bookId){
            return $http.get($rootScope.localApiUrl+'sendMessage',{bookid:bookId},).
           then(handleSuccess, handleError('send book message'));
        }

        function cateAndSubcatList(){
            return $http.get($rootScope.localApiUrl+'sampleData/textbookLeftSidecategory.json').
           then(handleSuccess, handleError('customeBroughtTextBooks'));
        }
        
        function customeBroughtTextBooks(slug){
           return $http.get($rootScope.localApiUrl+'sampleData/customeBoughtTextbook.json').
           then(handleSuccess, handleError('customeBroughtTextBooks'));
        }

        function GetBooks(lastpage) {
           return $http.get($rootScope.apiUrl+'/get-module/books?page='+lastpage).
           then(handleSuccess, handleError('Error getting all books')); 
           // return $http.get($rootScope.localApiUrl+'sampleData/textbookLanding.json').
           // then(handleSuccess, handleError('Error getting all books'));
        }

        function GetBook(slug) {
            return $http.get($rootScope.localApiUrl+'sampleData/singleTextBookDescription.json').
            then(handleSuccess, handleError('Error getting single books'));
        }

        function searchTextBook(searchText){
           return $http.get($rootScope.localApiUrl+'sampleData/customeBoughtTextbook.json').
           then(handleSuccess, handleError('customeBroughtTextBooks'));
        }

        function loadMoreTextBooks(lastpage){
            return $http.get($rootScope.apiUrl+'/get-module/books?page='+lastpage).
           then(handleSuccess, handleError('Error getting all books')); 
        }

    }

})();