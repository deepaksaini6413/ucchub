var app = angular.module('MainSrvc',[]);
app.factory("MAIN",function($http, $window){
	var token = $('meta[name="token"]').attr('content');
	return{
		getNotification: function(){
			return $http.post('api/get-notification?_token='+token);
		},
		getAllNotification: function(){
			return $http.post('api/get-all-notification?_token='+token);
		},
		seenNotification: function() {
			return $http.post('api/seen-notification?_token='+token);
		},
		getGroup: function(param) {
			return $http.post('api/get-group?_token='+token, param);
		},
		getChat: function(param) {
			return $http.post('api/get-chat?_token='+token, param);
		},
		sendMessage: function(param) {
			return $http.post('api/send-message?_token='+token, param);
		},
		removeImg: function() {
			return $http.post('api/remove-img?_token='+token);
		},
		getInbox: function() {
			return $http.post('api/get-inbox?_token='+token);
		},
		getArchive: function() {
			return $http.post('api/get-archive?_token='+token);
		},
		receiveMsg: function(param) {
			return $http.post('api/get-receive-msg-group?_token='+token, param);
		},
		deleteStream: function(param) {
			return $http.post('api/remove-stream?_token='+token, param);
		},
		makeArchive: function(param) {
			return $http.post('api/make-archive?_token='+token, param);
		},
		toInbox: function(param) {
			return $http.post('api/move-to-inbox?_token='+token, param);
		},
		askQuery: function(param) {
			return $http.post('api/ask-query?_token='+token, param);
		},
		getDiscussion: function(param) {
			return $http.post('api/get-discussion?_token='+token, param);
		},
		share: function(param) {
			return $http.post('api/share-job?_token='+token, param);
		},
		downloadFile: function(param) {
            $window.open('api/get-custom/'+param.id+'/job-file?_token='+token, '_blank');
            return true;
        },
		downloadMsgFile: function(param) {
            $window.open('api/get-custom/'+param.id+'/msg-file?_token='+token, '_blank');
            return true;
        },
        removeProfilePicture: function() {
        	return $http.post('api/remove-profile-picture?_token='+token);
        }
	}
});