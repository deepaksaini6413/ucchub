(function () {
    'use strict';

    angular
        .module('app')
        .factory('BookService', BookService);

    BookService.$inject = ['$rootScope','$http'];
    function BookService($rootScope, $http) {
        var service = {};

        service.Books       = Books;
        service.Book        = Book;
        service.Create      = Create;
        service.UserBooks   = UserBooks;
        service.Update      = Update;
        service.GetBooks = GetBooks;
        //initService();

        return service;

        // private functions

        function handleSuccess(res) {
            return res;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

        function GetBooks(lastpage) {
           return $http.get($rootScope.apiUrl+'/get-module/books?page='+lastpage).
           then(handleSuccess, handleError('Error getting all books')); 
           // return $http.get($rootScope.localApiUrl+'sampleData/textbookLanding.json').
           // then(handleSuccess, handleError('Error getting all books'));
        }

        function Books(lastpage) {
           return $http.get($rootScope.apiUrl+'/get-module/books?page='+lastpage).then(handleSuccess, handleError('Error getting all books'));
        }

        function Book(slug) {
            return $http.get($rootScope.apiUrl+'/get-single-module/books/'+slug).then(handleSuccess, handleError('Error getting single books'));
        }

        function Create(data) {
            return $http.post($rootScope.apiUrl+'/books/create',data).then(handleSuccess, handleError('Error getting single books'));
        }

        function UserBooks(lastpage) {
            return $http.get($rootScope.apiUrl+'/get-module/books?page='+lastpage).then(handleSuccess, handleError('Error getting all books'));
        }

        function Update(data){
            return $http.post($rootScope.apiUrl+'/books/'+data.id+'/update',data).then(handleSuccess, handleError('Error on updating book'));
        }
    }

})();