(function () {
    'use strict';

    angular
        .module('app')
        .factory('CollegeService', CollegeService);

    CollegeService.$inject = ['$rootScope','$http'];
    function CollegeService($rootScope, $http) {
        var service = {};

        service.Colleges = Colleges;
        service.College = College;
        service.AddToWishList = AddToWishList;

        service.leftNavigationColleges=leftNavigationColleges;
        service.sortCollegeByAlphabates=sortCollegeByAlphabates;
        service.beseColleges=beseColleges;
        service.searchCollegeByLocation = searchCollegeByLocation;
        service.loadTopScholarship  = loadTopScholarship;
        //initService();

        return service;

        // private functions

        function handleSuccess(res) {
            return res;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

        function loadTopScholarship(){
            return $http.get($rootScope.localApiUrl+'sampleData/topScholarship.json').
           then(handleSuccess, handleError('testimonialsTeam'));
        }

        function leftNavigationColleges() {
           return $http.get($rootScope.localApiUrl+'sampleData/leftSidenavigation.json').
           then(handleSuccess, handleError('left data not loading'));
        }

        function beseColleges(){
           return $http.get($rootScope.localApiUrl+'sampleData/bestCollegeData.json').
           then(handleSuccess, handleError('best College By Alphabates'));
        }

        function searchColleges(searchText){
            return $http.get($rootScope.apiUrl+'/get-module/colleges?searchText='+searchText).
            then(handleSuccess, handleError('No Data Found'));
        }

        function sortCollegeByAlphabates(lastpage) {
           return $http.get($rootScope.apiUrl+'/college-sort-data?sort=asend').
           then(handleSuccess, handleError('sort College By Alphabates'));
        }

        function Colleges(lastpage) {
           return $http.get($rootScope.apiUrl+'/get-module/colleges?page='+lastpage+"&best=true").then(handleSuccess, handleError('Error getting all books'));
        }

        function College(slug) {
            return $http.get($rootScope.apiUrl+'/get-single-module/colleges/'+slug).
            then(handleSuccess, handleError('Error getting single item'));
            // return $http.get($rootScope.localApiUrl+'sampleData/singleCollegeData.json').
            // then(handleSuccess, handleError('Error college getting single item'));
        }

        function AddToWishList(id) {
            return $http.get($rootScope.apiUrl+'/college/add-to-wishlist/'+id).then(handleSuccess, handleError('Error getting single item'));
        }

        function searchCollegeByLocation(data){
            return $http.post($rootScope.apiUrl+'/get-module/colleges?page=1',{city:data.cureentAddress}).then(handleSuccess, handleError('Error getting single item'));
        }
    }

})();