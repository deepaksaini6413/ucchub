(function () {
    'use strict';

    angular
        .module('app')
        .factory('EventService', EventService);

    EventService.$inject = ['$rootScope','$http'];
    function EventService($rootScope, $http) {
        var service = {};

        service.Events        = Events;
        service.Event         = Event;
        service.UserEvents    = UserEvents; 
        service.Create        = Create;
        service.Update        = Update;
        service.searchEvents  = searchEvents; 
        //initService();

        return service;

        // private functions

        function handleSuccess(res) {
            return res;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

        function Events(lastpage) {
           //  return $http.get($rootScope.localApiUrl+'sampleData/events.json').
           // then(handleSuccess, handleError('events'));
           return $http.get($rootScope.apiUrl+'/get-module/events?page='+lastpage).then(handleSuccess, handleError('Error getting all events'));
        }

        function Event(slug) {
            return $http.get($rootScope.apiUrl+'/get-single-module/events/'+slug).then(handleSuccess, handleError('Error getting single item'));
        }

        function Create(data) {
            return $http.post($rootScope.apiUrl+'/events/create',data).then(handleSuccess, handleError('Error getting single events'));
        }

        function UserEvents(lastpage){
            return $http.get($rootScope.apiUrl+'/get-module/events?page='+lastpage).then(handleSuccess, handleError('Error getting all events'));
        }

        function Update(data){
            return $http.post($rootScope.apiUrl+'/events/'+data.id+'/update',data).then(handleSuccess, handleError('Error on updating Job'));
        }

        function searchEvents(data,lastpage = 1){

            return $http.post($rootScope.apiUrl+'/events/search?page='+lastpage,data).then(handleSuccess, handleError('Error on search job searchEvents()'));
        }
    }

})();