﻿(function () {
    'use strict';

    var app = angular
        .module('app', ['ngRoute','ngSanitize','ngCookies','ngAnimate', 'ui.bootstrap','AxelSoft','ngToast'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'module/templates/home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'module/templates/login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/verification/',{
                templateUrl:'module/templates/login/verify.view.html',
                controller:'RegisterController',
                controllerAs: 'vm'
            })

            .when('/forget-password/',{
                templateUrl: 'module/templates/register/step-2.view.html',
                controller:'RegisterController',
                controllerAs: 'vm'
            })

            .when('/step-1', {
                controller: 'RegisterController',
                templateUrl: 'module/templates/register/step-1.view.html',
                controllerAs: 'vm'
            })

            .when('/forget-password', {
                controller: 'RegisterController',
                templateUrl: 'module/templates/register/forget-password.view.html',
                controllerAs: 'vm'
            })

            .when('/reset-password/:email/:verification_code', {
                controller: 'RegisterController',
                templateUrl: 'module/templates/register/reset-password.view.html',
                controllerAs: 'vm'
            })


            /*Books Route */
            .when('/book/:slug', {
                controller: 'BookController',
                templateUrl: 'module/templates/book/booksingle.view.html',
                controllerAs: 'vm'
            })

            /*College Routes*/
            .when('/colleges', {
                controller: 'CollegeController',
                templateUrl: 'module/templates/college/college.view.html',
                controllerAs: 'vm'
            })

            .when('/college/:slug', {
                controller: 'CollegeController',
                templateUrl: 'module/templates/college/collegesingle.view.html',
                controllerAs: 'vm'
            })
            
            /*News Routes*/
            .when('/news', {
                controller: 'NewsController',
                templateUrl: 'module/templates/news/news.view.html',
                controllerAs: 'vm'
            })
            
            .when('/news/:slug', {
                controller: 'NewsController',
                templateUrl: 'module/templates/news/newssingle.view.html',
                controllerAs: 'vm'
            })

            /*Events Routes*/
            .when('/events', {
                controller: 'EventController',
                templateUrl: 'module/templates/event/event.view.html',
                controllerAs: 'vm'
            })
            
            .when('/event/:slug', {
                controller: 'EventController',
                templateUrl: 'module/templates/event/eventsingle.view.html',
                controllerAs: 'vm'
            })

            .when('/search-events', {
                controller: 'EventController',
                templateUrl: 'module/templates/event/eventsearch.view.html',
                controllerAs: 'vm'
            })

            /*TextBook Routes*/
            .when('/textBooks', {
                controller: 'TextBookController',
                templateUrl: 'module/templates/textbook/textbooklnading.html',
                controllerAs: 'vm'
            })
            
            .when('/textBooks/:slug', {
                controller: 'TextBookController',
                templateUrl: 'module/templates/textbook/textbookProfilePage.html',
                controllerAs: 'vm'
            })

            .when('/textBooks/search/:slug', {
                controller: 'TextBookController',
                templateUrl: 'module/templates/textbook/textbookSearchPage.html',
                controllerAs: 'vm'
            })

            // .when('/textBooks/category/:slug', {
            //     controller: 'TextBookController',
            //     templateUrl: 'module/templates/textbook/textbookSearchPage.html',
            //     controllerAs: 'vm'
            // })

            /*Jobs Routes*/
            .when('/jobs', {
                controller: 'JobController',
                templateUrl: 'module/templates/job/job.view.html',
                controllerAs: 'vm'
            })

            .when('/job/:slug', {
                controller: 'JobController',
                templateUrl: 'module/templates/job/jobsingle.view.html',
                controllerAs: 'vm'
            })

            .when('/jobs/:slug/:id', {
                controller: 'JobController',
                templateUrl: 'module/templates/job/job.description.html',
                controllerAs: 'vm'
            })

            /*Jobs Routes*/
            .when('/scholarship', {
                controller: 'ScholarshipController',
                templateUrl: 'module/templates/scholarship/scholarship.view.html',
                controllerAs: 'vm'
            })

            .when('/scholarship/:slug', {
                controller: 'ScholarshipController',
                templateUrl: 'module/templates/scholarship/scholarshipsingle.view.html',
                controllerAs: 'vm'
            })

            /*Jobs Routes*/
            .when('/profile', {
                controller: 'UserController',
                templateUrl: 'module/templates/user/profile.view.html',
                controllerAs: 'vm'
            })

            /*Notification Routes*/
            .when('/notification', {
                controller: 'NotificationController',
                templateUrl: 'module/templates/notification/notification.html',
                controllerAs: 'vm'
            })
            

            //.otherwise({ redirectTo: '/login' });

    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http','UserService','AuthenticationService'];
    function run($rootScope, $location, $cookies, $http, UserService, AuthenticationService) {

        //Server 
        console.log($location.absUrl());
        var absUrl = $location.absUrl();
        $rootScope.urlPrefix = "#!/";
        if(absUrl.indexOf("localhost") !== -1)
        {
            //Localhost
            $rootScope.apiUrl = "http://localhost/ucclaravel/public/index.php/api";
            $rootScope.moduleThumbnailUrl = "http://localhost/ucclaravel/public/images/category/thumbnail/300/";
            $rootScope.moduleThumbnailUrl800 = "http://localhost/ucclaravel/public/images/category/thumbnail/800/";
            $rootScope.apiUrlWithoutIndex = "http://localhost/ucclaravel/public";

            $rootScope.localApiUrl="http://localhost/ucc/ucchub/module/";

            if(absUrl.indexOf("localhost:8080") !== -1 ){
                $rootScope.localApiUrl="http://localhost:8080/ucchub/module/";                
            }

        }else if(absUrl.indexOf("skws.in") !== -1){

            // Our server
            $rootScope.apiUrl = "http://skws.in/ucclaravel/public/api";
            $rootScope.moduleThumbnailUrl = "http://skws.in/ucclaravel/public/images/category/thumbnail/300/";

        }else{

            $rootScope.apiUrl = "http://174.138.42.98/ucclaravel/public/index.php/api";
            $rootScope.moduleThumbnailUrl = "http://174.138.42.98/ucclaravel/public/images/category/thumbnail/300/";
            $rootScope.moduleThumbnailUrl800 = "http://174.138.42.98/ucclaravel/public/images/category/thumbnail/800/";
            $rootScope.apiUrlWithoutIndex = "http://174.138.42.98/ucclaravel/public";
            $rootScope.localApiUrl="http://174.138.42.98/ucchub/module/";
        }
        
        $rootScope.noImageUrl = "images/no-photo.jpg";
        $rootScope.loaderImageUrl="images/loader.gif";
        $rootScope.isUserLoggedIn = false;
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};

        $rootScope.menuItems=[{
        "defaultMenu":['colleges','scholarship','news','events','jobs','textBooks'],
        "colleges":['colleges search','colleges profile'],
        "scholarship":['scholarship1','scholarship2'],
        "news":['news1','news2','news3','news4','news5'],
        "events":['events','events1','events2'],
        "jobs":['jobs1','jobs2'],
        "textbooks":['textbooks']
         }
        ];

        $rootScope.setUniversityCollege=[
        {id:1,name:"Abc university new nagar instnrjhdfashd njndksnhdhasd jsd"},
        {id:2,name:"reeta university"},
        {id:3,name:"geeta university"},
        {id:4,name:"sita university"},
        {id:5,name:"kunvar university"}];

        $rootScope.setUniversityCollegeName=$rootScope.setUniversityCollege[0].name;
        $rootScope.changeUniversityCollege = function(item){
            if(item.id > 0){
                $rootScope.setUniversityCollegeName=item.name;
                window.localStorage.setItem('selectedCollegeOrUniversity',item.id);
            }
        };

        $rootScope.setDRPMenuItem=['No Item Here'];

        $rootScope.setHeadeMenuItem = function(currentMenuItem,value){

            $rootScope.setDRPMenuItem=["colleges","scholarship","news","events","jobs","textBooks"];

            switch(currentMenuItem){
                case 'colleges':
                $rootScope.menuItems=[{
                  "defaultMenu":['colleges search','colleges profile']  
                }];

                window.location.href="#!/colleges";
                break;

                case 'scholarship':
                $rootScope.menuItems=[{
                  "defaultMenu":['scholarship','scholarship1','scholarship2']  
                }];
                window.location.href="#!/scholarship";
                break;

                case 'news':
                $rootScope.menuItems=[{
                  "defaultMenu":['news','news1','colleges3']  
                }];
                window.location.href="#!/news";
                break;

                case 'events':
                $rootScope.menuItems=[{
                  "defaultMenu":['events','events1','colleges3']  
                }];
                window.location.href="#!/events";
                break;

                case 'jobs':
                $rootScope.menuItems=[{
                  "defaultMenu":['jobs','colleges2','colleges3']  
                }];
                window.location.href="#!/jobs";
                break;

                case 'textBooks':
                $rootScope.menuItems=[{
                  "defaultMenu":['textBooks','textBooks1','colleges3']  
                }];
                window.location.href="#!/textBooks";
                break;
            }

           //  if(currentMenuItem == 'colleges'){
           //  $rootScope.menuItems=[{
           //    "defaultMenu":['colleges1','colleges2','colleges3']  
           //  }];

            
           // }
           // else if(currentMenuItem == 'scholarship'){
           //  $rootScope.menuItems=[{
           //    "defaultMenu":['scholarship','scholarship1','scholarship2','scholarship3','scholarship4'] 
           //  }];
           // }

        };

        if ($rootScope.globals.currentUser) {
            $rootScope.name = $rootScope.globals.currentUser.name;
            $rootScope.isUserLoggedIn = true;
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.authdata;
        }

        

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/step-1', '/forget-password' , '/reset-password', '/verification']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                //$location.path('/login');
            }else{
                //$rootScope.isUserLoggedIn = true;
            }
            
        });

        $rootScope.logout = function(){
            AuthenticationService.ClearCredentials();
            $rootScope.isUserLoggedIn = false;
            console.log($rootScope.isUserLoggedIn);
            $location.path('/');
            
        };

        $rootScope.seeNotifications = function(){
            $location.path('/notification');
        }

    }

    app.filter('extractFirstAndSecondLetter', function() {
        return function(input) {
          var split = input.split(" ");

          var response;
          if(split.length >= 2)
          {
            response = split[0].charAt(0) + split[1].charAt(0);
          }

          if(split.length == 1)
          {
            response = split[0].substring(0,2);
          }
          return response.toUpperCase();
        }
    });
})();